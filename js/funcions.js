
function switch_img (m, n) {

   for (i = 0; i < n; i++) {
      document.getElementById ('img'+i).style.display = 'none';
      document.getElementById ('min'+i).className = 'BotigaMini';
   }
   document.getElementById ('img'+m).style.display = 'block';
   document.getElementById ('min'+m).className = 'BotigaMiniAct';

}


function obre_cerca (sbus, snot) {
   if ( (document.getElementById ('cerc').keyw.value == sbus) ||
        (/^\s*$/.test(document.getElementById ('cerc').keyw.value)) ) {
      alert(snot);
      return false;
   } else {
      return true;
   }
}


function have_pos (obj) {

   var curleft = curtop = 0;

   if (obj.offsetParent) {

      do {
         curleft += obj.offsetLeft;
         curtop  += obj.offsetTop;
      } while (obj = obj.offsetParent);

      return [curleft,curtop];

   }

}


function initMouseOverMenu () {
   var div = document.getElementById ('menu2');
   div.mouseOver = false;
   div.onmouseover = function () {
      this.mouseOver = true;
   };
   div.onmouseout = function () {
      this.style.display = 'none';
      this.mouseOver = false;
   };
   pos = have_pos (document.getElementById ('opcio2'));
   div.style.top  = (pos[1] + 23) + 'px';
   div.style.left = pos[0] + 'px';
}


/*window.onload = function () { initMouseOverMenu (); }*/


$(document).ready (function () {
   $('.BotigaProdEsquerraFami').click (function () {
      fact = $(this).attr('id');
      pact = fact.replace (/Head/, 'Body');
      $('.BotigaProdEsquerraProductes').hide ('slow');
      $('#'+pact).show ('slow');
   });
   $('.BotigaProdDcap').click (function () {
      hact = $(this).attr('id');
      nact = hact.replace (/prodCap/, '');
      for (i = 0; i < 4; i++) {
         if ( i == nact ) {
            if ( document.getElementById ('prodCos'+i).style.display == 'none' ) {
               document.getElementById ('imgCap'+i).src = '../../../../../img/sect_act.gif';
               document.getElementById ('prodCap'+i).style.cursor = '';
               $('#prodCos'+i).slideDown ('slow');
            }
         } else {
            if ( document.getElementById ('prodCos'+i).style.display == 'block' ) {
               document.getElementById ('imgCap'+i).src = '../../../../../img/none.gif';
               document.getElementById ('prodCap'+i).style.cursor = 'pointer';
               document.getElementById ('prodCos'+i).style.display = 'none';
            }
         }
      }
   });
});
$(function() {
   anchors = document.getElementsByTagName ('a');
   labels = new Array ();
   for (i = 0; i < anchors.length; i++) {
      rel = anchors[i].getAttribute ('rel');
      if ( /^lightbox/.test (rel) ) {
         nou = true;
         for (l = 0; l < labels.length; l++) {
            if ( labels[l] == rel ) {
               nou = false;
               break;
            }
         }
         if ( nou ) {
            $('a[rel*='+rel+']').lightBox ();
            labels.push (rel);
         } 
      }
   }
});

