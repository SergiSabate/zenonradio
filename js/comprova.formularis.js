<!-- 
/*
Funcions:

	- EsNumerico (obj) :  bool
	- NumDigitos (obj,lon) : bool
	- esta_ple (obj) : bool
	- mira_email (obj) : bool
	- testeja_check(obj,lon) : bool
	- data_valida(obj): bool
	- hora_valida(obj): bool

*/




function EsNumerico(idobj){
/* Torna
	0: no �s num�ric
	1: �s num�ric
*/
   var chr;
   if (idobj.value==null || idobj.value.length==0) { return false; }
   for (var i=0;i<idobj.value.length;i++){
     chr=idobj.value.substring(i,i+1);
     if (chr<"0"||chr>"9") return 0;
   }
   return 1;
}

/*Aquesta funcio comprova que els camps per al formulari del domini*/

function sonlletres(idobj){
/* Torna
	0: Incorrecte
	1: Correcte
*/ 
   var chr;
if (idobj.value==null || idobj.value.length==0) { return false; }
   for (var i=0;i<idobj.value.length;i++){
     chr=idobj.value.substring(i,i+1);
     if (majuscules(chr)==0&&minuscules(chr)==0&&numeros(chr)==0&&punt(chr)==0&&espai(chr)==0&&guio(chr)==0&&guiobaix(chr)==0) return 0;
     }

    return 1;
}

function passwdvalid(idobj){
/* Torna
	0: Incorrecte
	1: Correcte
*/ 

   var chr;
if (idobj.value==null || idobj.value.length==0) { return false; }
   for (var i=0;i<idobj.value.length;i++){
     chr=idobj.value.substring(i,i+1);
     if (majuscules(chr)==0&&minuscules(chr)==0&&numeros(chr)==0&&punt(chr)==0&&barra(chr)==0&&guio(chr)==0&&guiobaix(chr)==0) return 0;
     }

    return 1;
}

/*Mira si hi ha un punt o no*/
function punt(lletra)
{

if(lletra!='.') return 0;
return 1;
}

/*Mira si hi ha una barra o no*/
function barra(lletra)
{

if(lletra!='/') return 0;
return 1;
}

/* Mira si hi ha un espai*/
function espai(lletra)
{
if(lletra!=' ') return 0;
return 1;
}

/*Mira si hi ha un guio */
function guio(lletra)
{
if(lletra!='-') return 0;
return 1;
}

/*Mira su hi ha un gui baix o no*/
function guiobaix(lletra)
{
if(lletra!='_') return 0;
return 1;
}

/*Mira si es majuscula*/
function majuscules(lletra)
{
if(lletra<"A"||lletra>"Z") return 0;
return 1;
}

/* MIra si es minuscula*/
function minuscules(lletra)
{
if(lletra<"a"||lletra>"z") return 0;
return 1;
}

/*Mira si hi ha numeros*/
function numeros(lletra)
{
if(lletra<"0"||lletra>"9") return 0;
return 1;
}



function caracter(obj)
{
lletres = new String();
comenca = 0;

for(i=0;i<obj.value.length;i++)
{

 if(obj.value.charAt(i) <= "a" || obj.value.charAt(i) >= "Z")
 {

   lletres=obj.value.substring(comenca,i);
   comenca = i + 1;

  }
  else
  {
    return false;
  }

 }


return true;
}


function NumDigitos(idobj,longitud){
/* Torna
	0: no t� n d�gits
	1: t� n d�gits
�ssent n = longitud

*/
   var chr;
   if (idobj.value.length != longitud) { return 0; }
   else { return 1; }
}


function alerta_1 (str)
{
	alert("Faltan sus datos en el campo " + str + " ...");
}


function alerta_ar (str)
{
	alert("Check the field " + str + " ...");
}

function alerta_ct (str)
{
	alert("Revisa el camp " + str + " ...");
}

function alerta_de (str)                                                        
{
        //alert("�berpr�fen Sie das Feld " + str + " ...");                         
        alert("\u00DCberpr\u00FCfen Sie das Feld " + str + " ...");                         
}

function alerta_en (str)
{
	alert("Check the field " + str + " ...");
}

function alerta_es (str)
{
	alert("Revisa el campo " + str + " ...");
}

function alerta_fr (str)
{
	//alert("Contr�lez le champ " + str + " ...");
	alert("Contr\u00F4lez le champ " + str + " ...");
}

function alerta_it (str)
{
        alert("Controllare il campo " + str + " ...");
}

function alerta_tr (str)
{
	alert("Check the field " + str + " ...");
}


function esta_ple(d1)
{
/* Torna
	0: Camp Buit
	1: Camp Ple
*/
	if (d1.value=='')
		return 0;
	else
		return 1;
}




function mira_email_old(adrecaemail)
{
/* Torna
	0: no hi ha res
	1: si es correcte
	9: email incorrecte
*/
	result=esta_ple(adrecaemail);
	if(result == 1)
	{
		if(adrecaemail.value.indexOf('@', 0) == -1)
		{
			return 9;
		}
		else
		{
        return 1;
		}
	}
	else
	{ 
		return 0;
    }
}


function mira_email(email) {
/* Torna
        0: no hi ha res
        1: si es correcte
        9: email incorrecte
*/

if(email.value!="")
   {
   if (/^[A-Za-z0-9\.+_-]+@[A-Za-z0-9\.-]+\.[A-Za-z]{2,6}$/.test(email.value))
  	{
	return 1;
	}
	else
	{
	return 9;
	}
   }
else
   {
	return 0;
   }
}


function comprova_telefon (telf) {

   if (/^\s*\+?[\s0-9]+$/.test(telf.value)) {
      return true;
   } else {
      return false;
   }

}


function comprova_radio(que,longitud)
{

/*
        Retorna 0 si no hi ha cap seleccio
        Retorna el numero d'ordre de la seleccionada (comen�ant per 1)
*/
                value=0;
                for(i=0;i<longitud;i++)
                {

                        if(que[i].checked == true)
                        {
                                value=i+1;
                        }
                }
                return value;
}

function comprova_checkbox(que,longitud)
{

/*
Retorna un array value[]

valor[0] = 0 si no hi ha cap seleccionat
valor[0] = 1 si hi ha res seleccionat
la resta del array son els numero d'ordre dels valors seleccionats
(comen�ant per 0)
*/
		valor=new Array();
                valor[0]=0;
		j=1
                for(i=0;i<longitud;i++)
                {
                        if(que[i].checked == true)
                        {
				valor[j] = i;
                                valor[0]++;
				j++ ;
                        }
                }
                return valor;
}


function longitud_checkbox(que,longitud)
{

/*
Compta quants checkbox estan seleccionats
*/

		valor=new Array();
                valor[0]=0;
		comptador=0;
                for(i=0;i<longitud;i++)
                {

                        if(que[i].checked == true)
                        {
				comptador++;
                        }
                }
                return comptador;
}

/***** data_valida *********************************************************
 *
 * El format de la data ha de ser dd/mm/aaaa, el separador pot ser qualsevol.
 *
 * retorna bool.
 *
 ***************************************************************************/

function data_valida(obj)
{

if (obj.value=='') {
   return false;
}

if (obj.value.length<10) {
   return false;
}

separador=obj.value[2];
dia=obj.value.charAt(0) + obj.value.charAt(1);
mes=obj.value.charAt(3) + obj.value.charAt(4);
mes--;
any=obj.value.charAt(6) + obj.value.charAt(7) + obj.value.charAt(8) + obj.value.charAt(9);

data = new Date(any,mes,dia);
  //      && (any==(data.getYear()+1900)))

if(        (dia==data.getDate())
        && (mes==data.getMonth())
        && (any==(data.getFullYear())))
{
return true;
}

obj.focus();
return false;
}


/***** hora_valida *********************************************************
 *
 * El format de l'hora ha de ser hh:mm (pot ser --:--)
 *
 * retorna bool.
 *
 ***************************************************************************/

function hora_valida (obj) {

   if ( obj.value == '--:--' ) {
      return true;
   } else {
      if ( /^(0(0|1|2|3|4|5|6|7|8|9)|1(0|1|2|3|4|5|6|7|8|9)|2(0|1|2|3)):[012345][0-9]$/.test (obj.value) ) {
         return true;
      }
   }

   return false;

}


function  hihaselect(obj)
{
if(obj[obj.selectedIndex].value=='')
{
return "0";
}
else
{
return "1";
}

}


function SonNumerets(idobj)
{
/* Torna
        0: no �s num�ric
        1: �s num�ric
*/
   var chr;
   for (var i=0;i<idobj.value.length;i++){
     chr=idobj.value.substring(i,i+1);
     if (chr<"0"||chr>"9") return 0;
   }
   return 1;
}

function sonlletresAZ(idobj){
/* Torna
	0: Incorrecte
	1: Correcte
*/ 
   var chr;
if (idobj.value==null || idobj.value.length==0) { return false; }
   for (var i=0;i<idobj.value.length;i++){
     chr=idobj.value.substring(i,i+1);
     if (majuscules(chr)==0&&minuscules(chr)==0&&punt(chr)==0&&espai(chr)==0&&guio(chr)==0&&guiobaix(chr)==0) return 0;
     }

    return 1;
}
